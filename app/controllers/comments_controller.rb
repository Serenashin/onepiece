class CommentsController < ApplicationController
	before_action :set_comment, only: [:destroy]
  def create
  	@comment = current_user.comments.build(comment_params)
  	@comment.save

  	redirect_to @comment.recipe
  end

  def destroy
    @recipe = @comment.recipe
    if @comment.user == current_user
    	@comment.destroy
    end 

  	redirect_to @recipe
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:content, :user_id, :recipe_id)
    end

end
